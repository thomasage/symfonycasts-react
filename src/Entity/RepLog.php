<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * RepLog.
 *
 * @ORM\Table(name="rep_log")
 * @ORM\Entity(repositoryClass="App\Repository\RepLogRepository")
 */
class RepLog
{
    public const ITEM_LABEL_PREFIX = 'liftable_thing.';
    public const WEIGHT_FAT_CAT = 18;

    /**
     * @var array<string,float>
     */
    private static $thingsYouCanLift = [
        'cat' => 9,
        'fat_cat' => self::WEIGHT_FAT_CAT,
        'laptop' => 4.5,
        'coffee_cup' => .5,
    ];

    /**
     * @var int|null
     *
     * @Serializer\Groups({"Default"})
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @Serializer\Groups({"Default"})
     * @ORM\Column(name="reps", type="integer")
     * @Assert\NotBlank(message="How many times did you lift this?")
     * @Assert\GreaterThan(value=0, message="You can certainly life more than just 0!")
     */
    private $reps;

    /**
     * @var string
     *
     * @Serializer\Groups({"Default"})
     * @ORM\Column(name="item", type="string", length=50)
     * @Assert\NotBlank(message="What did you lift?")
     */
    private $item;

    /**
     * @var float
     *
     * @Serializer\Groups({"Default"})
     * @ORM\Column(name="totalWeightLifted", type="float")
     */
    private $totalWeightLifted;

    /**
     * The user who lifted these items.
     *
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=false)
     */
    private $user;

    /**
     * Returns an array that an be used in a form drop down.
     *
     * @return array<string,string>
     */
    public static function getThingsYouCanLiftChoices(): array
    {
        $things = array_keys(self::$thingsYouCanLift);
        $choices = [];
        foreach ($things as $thingKey) {
            $choices[self::ITEM_LABEL_PREFIX.$thingKey] = $thingKey;
        }

        return $choices;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getReps(): int
    {
        return $this->reps;
    }

    public function setReps(int $reps): self
    {
        $this->reps = $reps;
        $this->calculateTotalLifted();

        return $this;
    }

    /**
     * Calculates the total weight lifted and sets it on the property.
     */
    private function calculateTotalLifted(): void
    {
        if (!$this->getItem()) {
            return;
        }

        $weight = self::$thingsYouCanLift[$this->getItem()];
        $totalWeight = $weight * $this->getReps();

        $this->totalWeightLifted = $totalWeight;
    }

    /**
     * Get item.
     */
    public function getItem(): string
    {
        return $this->item;
    }

    /**
     * Set item.
     *
     * @param string $item
     */
    public function setItem($item): RepLog
    {
        if (!isset(self::$thingsYouCanLift[$item])) {
            throw new \InvalidArgumentException(sprintf('You can\'t lift a "%s"!', $item));
        }

        $this->item = $item;
        $this->calculateTotalLifted();

        return $this;
    }

    public function getItemLabel(): string
    {
        return self::ITEM_LABEL_PREFIX.$this->getItem();
    }

    /**
     * Get totalWeightLifted.
     */
    public function getTotalWeightLifted(): float
    {
        return $this->totalWeightLifted;
    }

    public function getUser(): User
    {
        return $this->user;
    }

    public function setUser(User $user): void
    {
        $this->user = $user;
    }
}
