<?php

namespace App\Controller;

use App\Entity\RepLog;
use App\Repository\RepLogRepository;
use App\Repository\UserRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Contracts\Translation\TranslatorInterface;

class LiftController extends BaseController
{
    /**
     * @Route("/lift", name="lift")
     */
    public function indexAction(
        Request $request,
        RepLogRepository $replogRepo,
        UserRepository $userRepo,
        TranslatorInterface $translator
    ): Response {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_REMEMBERED');

        $repLogAppProps = [
            'itemOptions' => [],
            'withHeart' => true,
        ];
        foreach (RepLog::getThingsYouCanLiftChoices() as $label => $id) {
            $repLogAppProps['itemOptions'][] = [
                'id' => $id,
                'text' => $translator->trans($label),
            ];
        }

        return $this->render(
            'lift/index.html.twig',
            [
                'leaderboard' => $this->getLeaders($replogRepo, $userRepo),
                'repLogAppProps' => $repLogAppProps,
            ]
        );
    }

    /**
     * Returns an array of leader information.
     *
     * @return array<array{username:string,weight:float,in_cats:string}>
     */
    private function getLeaders(RepLogRepository $replogRepo, UserRepository $userRepo): array
    {
        $leaderboardDetails = $replogRepo->getLeaderboardDetails();

        $leaderboard = [];
        foreach ($leaderboardDetails as $details) {
            if (!$user = $userRepo->find($details['user_id'])) {
                // interesting, this user is missing...
                continue;
            }

            $leaderboard[] = [
                'username' => $user->getUsername(),
                'weight' => $details['weightSum'],
                'in_cats' => number_format($details['weightSum'] / RepLog::WEIGHT_FAT_CAT),
            ];
        }

        return $leaderboard;
    }
}
