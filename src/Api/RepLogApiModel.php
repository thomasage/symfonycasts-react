<?php

declare(strict_types=1);

namespace App\Api;

final class RepLogApiModel
{
    /**
     * @var int
     */
    public $id;

    /**
     * @var int
     */
    public $reps;

    /**
     * @var string
     */
    public $itemLabel;

    /**
     * @var float
     */
    public $totalWeightLifted;

    /**
     * @var array<string,string>
     */
    private $links = [];

    public function addLink(string $ref, string $url): void
    {
        $this->links[$ref] = $url;
    }

    /**
     * @return array<string,string>
     */
    public function getLinks(): array
    {
        return $this->links;
    }
}
