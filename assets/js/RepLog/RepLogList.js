import React from 'react'
import PropTypes from 'prop-types'

export default function RepLogList ({
  highlightedRowId,
  isLoaded,
  isSavingNewRepLog,
  onDeleteRepLog,
  onRowClick,
  repLogs
}) {
  const handleDeleteClick = (event, repLogId) => {
    event.preventDefault()
    onDeleteRepLog(repLogId)
  }

  if (!isLoaded) {
    return (
        <tbody>
        <tr>
          <td className="text-center" colSpan="4">Loading...</td>
        </tr>
        </tbody>
    )
  }

  return (
      <tbody>
      {repLogs.map(repLog => (
          <tr key={repLog.id}
              className={highlightedRowId === repLog.id ? 'info' : ''}
              onClick={() => onRowClick(repLog.id)}
              style={{opacity: repLog.isDeleting ? 0.3 : 1}}>
            <td>{repLog.itemLabel}</td>
            <td>{repLog.reps}</td>
            <td>{repLog.totalWeightLifted}</td>
            <td>
              <a href="#" onClick={event => handleDeleteClick(event, repLog.id)}>
                <span className="fa fa-trash"/>
              </a>
            </td>
          </tr>
      ))}
      {isSavingNewRepLog && (
          <tr>
            <td colSpan="4"
                className="text-center"
                style={{ opacity: 0.5 }}>
              Lifting to the database...
            </td>
          </tr>
      )}
      </tbody>
  )
}

RepLogList.propTypes = {
  highlightedRowId: PropTypes.any,
  isLoaded: PropTypes.bool.isRequired,
  isSavingNewRepLog: PropTypes.bool.isRequired,
  onDeleteRepLog: PropTypes.func.isRequired,
  repLogs: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.number.isRequired,
    itemLabel: PropTypes.string.isRequired,
    reps: PropTypes.number.isRequired,
    totalWeightLifted: PropTypes.number.isRequired
  })),
  onRowClick: PropTypes.func.isRequired
}
